<html>
	<head>

		<style>
			html,
			body { margin: 0; font-family: "helvetica neue", helvetica, sans-serif; font-weight: 200; }

			h2 { padding: 0 0 60px 0; margin: 0 0 60px 0; color: #fff; font-size: 45px; font-weight: 500; border-bottom: 1px solid #999; }
			p { line-height: 35px; padding: 0; margin: 0 0 32px 0; font-size: 24px; }
			p:last-child { margin: 0; }

			a { color: #999; text-decoration: none; }

			ul { list-style: none; padding: 0; margin: 0; }
			ul li { line-height: 35px; }

			.section { width: 100%; height: 100%; position: relative; }
			.container { width: 100%; max-width: 1080px; position: relative; margin: 0 auto; }
			.wrap { display: table; width: 100%; height: 100%; box-sizing: border-box; }
			.inner-wrap { display: table-cell; width: 100%; height: 100%; vertical-align: middle; }

			.grid { list-style: none; padding: 0 0 60px 0; margin: 0 0 80px 0; font-size: 0; border-bottom: 1px solid #999; }
			.grid > li { display: inline-block; vertical-align: top; padding: 0 50px; font-size: 24px; box-sizing: border-box; }
			.grid > li.g50 { width: 50%; }

			#video video { position: fixed; right: 0; bottom: 0; min-width: 100%; min-height: 100%; width: auto; height: auto; z-index: -100; background: url(polina.jpg) no-repeat; background-size: cover; }
			#mask { width: 100%; height: 100%; position: absolute; top: 0; left: 0; background: url('/assets/img/mask.png') center center no-repeat; background-size: cover; }
			#main-content { background: #000; text-align: center; color: #999; }
			#main-content .wrap { padding-top: 50px; }
		</style>

	</head>
	<body>

		<div id="intro">
			
		</div>
		
		<div id="video" class="section">
			<video autoplay loop>
				<source src="/assets/video/movie.mp4" type="video/mp4">
				<source src="/assets/video/movie.ogg" type="video/ogg">
				Your browser does not support the video tag.
			</video>
		</div>

		<div id="mask"></div>

		<main id="main-content" class="section">
			<div class="wrap">
				<div class="inner-wrap">
					<div class="container">
						<ul class="grid">
							<li class="g50">
								<h2>Sparkservice</h2>
								<ul>
									<li>Branding</li>
									<li>Web</li>
									<li>Digital</li>
									<li>3d and visualisation</li>
									<li>Moving image</li>
									<li>Print</li>
									<li>Copywriting</li>
									<li>Thinking</li>
								</ul>
							</li>
							<li class="g50">
								<h2>Intheloop</h2>
								<p>18 Compton Terrace<br/>London N1 2UN</p>
								<p><a href="#">Map</a></p>
								<p>T. 020 7359 8383<br/><a href="hula@sparkloop.com">hula@sparkloop.com</a><br/><a href="jobs@sparkloop.com">jobs@sparkloop.com</a></p>
							</li>
						</ul>
						<h3>Bright ideas. Over and Over</h3>
					</div>
				</div>
			</div>
		</main>
		
	</body>
</html>
